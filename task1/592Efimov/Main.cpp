#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <algorithm>


MeshPtr makeHyperboloid(float radius, unsigned int M) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (unsigned int i = 0; i < M; i++) {
        float z = -1 + (float) i / M * 2;
        float z1 = z + 2.0f / M;
        float r = sqrt(1 + z * z);
        float r1 = sqrt(1 + z1 * z1);

        for (unsigned int j = 0; j < M; j++) {
            float phi = 2.0f * (float) glm::pi<float>() * j / M;
            float phi1 = phi + 2.0f * (float) glm::pi<float>() / M;

            // N = df/dx * i ; df/dy * j ; df/dz * k    where f(x,y,z) = x^2+y^2-z^2-1

            
            vertices.push_back(glm::vec3(cos(phi) * r, sin(phi) * r, z));
            vertices.push_back(glm::vec3(cos(phi1) * r, sin(phi1) * r, z));
            vertices.push_back(glm::vec3(cos(phi) * r1, sin(phi) * r1, z1));

            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r, 2. * sin(phi) * r, -2. * z)));
            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r, 2. * sin(phi1) * r, -2. * z)));
            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r1, 2. * sin(phi) * r1, -2. * z1)));

           
            vertices.push_back(glm::vec3(cos(phi1) * r1, sin(phi1) * r1, z1));
            vertices.push_back(glm::vec3(cos(phi1) * r, sin(phi1) * r, z));
            vertices.push_back(glm::vec3(cos(phi) * r1, sin(phi) * r1, z1));

            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r1, 2. * sin(phi1) * r1, -2. * z1)));
            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r, 2. * sin(phi1) * r, -2. * z)));
            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r1, 2. * sin(phi) * r1, -2. * z1)));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

class SampleApplication : public Application
{
public:
    MeshPtr _hyperboloid;

    ShaderProgramPtr _shader;

    float _periodicity = 50.0;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _hyperboloid = makeHyperboloid(0.5f, static_cast<unsigned int>(_periodicity));
      
        _shader = std::make_shared<ShaderProgram>("592EfimovData1/shader.vert", "592EfimovData1/shader.frag");
    }

    void update() override
    {
        double dt = glfwGetTime() - _oldTime;

        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
            _periodicity -= 100.0f * dt;
            _periodicity = std::max(4.0f, _periodicity);
            std::cout << _periodicity << std::endl;
            _hyperboloid = makeHyperboloid(0.5f, static_cast<unsigned int>(_periodicity));
        }
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
            _periodicity += 100.0f * dt;
            _periodicity = std::min(100.0f, _periodicity);
            std::cout << _periodicity << std::endl;
            _hyperboloid = makeHyperboloid(0.5f, static_cast<unsigned int>(_periodicity));
        }
        Application::update();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        
        _shader->use();

        
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        
        _shader->setMat4Uniform("modelMatrix", _hyperboloid->modelMatrix());
        _hyperboloid->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
