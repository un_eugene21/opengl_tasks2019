#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <sstream>
#include <vector>

#include <chrono>


MeshPtr makeHyperboloid(float radius, unsigned int M = 100) {
  std::vector<glm::vec3> vertices_1;
  std::vector<glm::vec3> vertices_2;
  std::vector<glm::vec3> normals_1;
  std::vector<glm::vec3> normals_2;
  std::vector<glm::vec2> texcoords;

  for (unsigned int i = 0; i < M; i++) {
    float z = -1 + (float) i / M * 2;
    float z1 = z + 2.0f / M;
    float r = sqrt(1 + z * z);
    float r1 = sqrt(1 + z1 * z1);
    const float rad = 1;

    for (unsigned int j = 0; j < M; j++) {
      float phi = 2.0f * (float) glm::pi<float>() * j / M;
      float phi1 = phi + 2.0f * (float) glm::pi<float>() / M;

      // N1 = df/dx * i ; df/dy * j ; df/dz * k    where f1(x,y,z) = x^2+y^2-z^2-1
      // N2 = df/dx * i ; df/dy * j ; df/dz * k    where f2(x,y,z) = x^2+y^2-1

      vertices_1.push_back(glm::vec3(cos(phi) * r, sin(phi) * r, z));
      vertices_1.push_back(glm::vec3(cos(phi1) * r, sin(phi1) * r, z));
      vertices_1.push_back(glm::vec3(cos(phi) * r1, sin(phi) * r1, z1));

      vertices_2.push_back(glm::vec3(cos(phi) * rad, sin(phi) * rad, z));
      vertices_2.push_back(glm::vec3(cos(phi1) * rad, sin(phi1) * rad, z));
      vertices_2.push_back(glm::vec3(cos(phi) * rad, sin(phi) * rad, z1));

      normals_1.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r, 2. * sin(phi) * r, -2. * z)));
      normals_1.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r, 2. * sin(phi1) * r, -2. * z)));
      normals_1.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r1, 2. * sin(phi) * r1, -2. * z1)));

      normals_2.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r, 2. * sin(phi) * r, 0)));
      normals_2.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r, 2. * sin(phi1) * r, 0)));
      normals_2.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r1, 2. * sin(phi) * r1, 0)));

      texcoords.push_back(glm::vec2((float) j / M, (float) i / M));
      texcoords.push_back(glm::vec2((float) (j + 1) / M, (float) i / M));
      texcoords.push_back(glm::vec2((float) j / M, (float) (i + 1) / M));

      vertices_1.push_back(glm::vec3(cos(phi1) * r1, sin(phi1) * r1, z1));
      vertices_1.push_back(glm::vec3(cos(phi) * r1, sin(phi) * r1, z1));
      vertices_1.push_back(glm::vec3(cos(phi1) * r, sin(phi1) * r, z));

      vertices_2.push_back(glm::vec3(cos(phi1) * rad, sin(phi1) * rad, z1));
      vertices_2.push_back(glm::vec3(cos(phi) * rad, sin(phi) * rad, z1));
      vertices_2.push_back(glm::vec3(cos(phi1) * rad, sin(phi1) * rad, z));

      normals_1.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r1, 2. * sin(phi1) * r1, -2. * z1)));
      normals_1.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r1, 2. * sin(phi) * r1, -2. * z1)));
      normals_1.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r, 2. * sin(phi1) * r, -2. * z)));

      normals_2.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r1, 2. * sin(phi1) * r1, 0)));
      normals_2.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r1, 2. * sin(phi) * r1, 0)));
      normals_2.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r, 2. * sin(phi1) * r, 0)));

      texcoords.push_back(glm::vec2((float) (j + 1) / M, (float) (i + 1) / M));
      texcoords.push_back(glm::vec2((float) j / M, (float) (i + 1) / M));
      texcoords.push_back(glm::vec2((float) (j + 1) / M, (float) i / M));
    }
  }

  DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf0->setData(vertices_1.size() * sizeof(float) * 3, vertices_1.data());

  DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf1->setData(normals_1.size() * sizeof(float) * 3, normals_1.data());

  DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

  DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf3->setData(vertices_2.size() * sizeof(float) * 3, vertices_2.data());

  DataBufferPtr buf4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf4->setData(normals_2.size() * sizeof(float) * 3, normals_2.data());

  MeshPtr mesh = std::make_shared<Mesh>();
  mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
  mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
  mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
  mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
  mesh->setAttribute(4, 3, GL_FLOAT, GL_FALSE, 0, 0, buf4);
  mesh->setPrimitiveType(GL_TRIANGLES);
  mesh->setVertexCount(vertices_1.size());

  return mesh;
}


class SampleApplication : public Application
{
public:
    MeshPtr _hyperboloid;

    MeshPtr _marker;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    float _lr;
    float _phi;
    float _theta;

    LightInfo _light;

    TexturePtr _worldTexture;

    GLuint _sampler;

    void makeScene() override
    {
        Application::makeScene();


        _hyperboloid = makeHyperboloid(0.5f);
        _hyperboloid->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _marker = makeSphere(0.1f);



        _shader = std::make_shared<ShaderProgram>();
        _shader->createProgram("592EfimovData2/texture.vert", "592EfimovData2/texture.frag");

        _markerShader = std::make_shared<ShaderProgram>();
        _markerShader->createProgram("592EfimovData2/marker.vert", "592EfimovData2/marker.frag");

        //=========================================================

        _lr = 3.0;
        _phi = 0.0;
        _theta = glm::pi<float>() * 0.25f;

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================

        _worldTexture = loadTexture("592EfimovData2/brick.jpg");

        //=========================================================

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override
    {

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _worldTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);


        {
            _shader->setMat4Uniform("modelMatrix", _hyperboloid->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _hyperboloid->modelMatrix()))));

            using namespace std::chrono;
            milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
            float phase = (sin(ms.count() / 1000.0));
            _shader->setFloatUniform("phase", phase);

            _hyperboloid->draw();
        }

        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
